Feature: Sale
  As a vendor
  I want to input a sale
  To get the total price

  Scenario: Sale 2 items with a price of 10.0 in UT state
    Given a sale with "2" items with a price of "10.0" in "UT" state
    When ask for total price
    Then return "21.37"

  Scenario: Sale 2 items with a price of 10.0 in NV state
    Given a sale with "2" items with a price of "10.0" in "NV" state
    When ask for total price
    Then return "21.6"

  Scenario: Sale 2 items with a price of 10.0 in TX state
    Given a sale with "2" items with a price of "10.0" in "TX" state
    When ask for total price
    Then return "21.25"

  Scenario: Sale 2 items with a price of 10.0 in AL state
    Given a sale with "2" items with a price of "10.0" in "AL" state
    When ask for total price
    Then return "20.8"

  Scenario: Sale 2 items with a price of 10.0 in CA state
    Given a sale with "2" items with a price of "10.0" in "CA" state
    When ask for total price
    Then return "21.65"

  Scenario: Sale 10 items with a price of 100.0 in UT state
    Given a sale with "10" items with a price of "100.0" in "UT" state
    When ask for total price
    Then return "1036.45"

  Scenario: Sale 10 items with a price of 100.0 in NV state
    Given a sale with "10" items with a price of "100.0" in "NV" state
    When ask for total price
    Then return "1047.6"

  Scenario: Sale 10 items with a price of 100.0 in TX state
    Given a sale with "10" items with a price of "100.0" in "TX" state
    When ask for total price
    Then return "1030.63"

  Scenario: Sale 10 items with a price of 100.0 in AL state
    Given a sale with "10" items with a price of "100.0" in "AL" state
    When ask for total price
    Then return "1008.8"

  Scenario: Sale 10 items with a price of 100.0 in CA state
    Given a sale with "10" items with a price of "100.0" in "CA" state
    When ask for total price
    Then return "1050.03"