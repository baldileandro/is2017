Given(/^a sale with "([^"]*)" items with a price of "([^"]*)" in "([^"]*)" state$/) do |quantity, unit_price, state_code|
  @sale = Sale.new(quantity.to_f, unit_price.to_f, state_code)
end

When(/^ask for total price$/) do
  @total = @sale.get_total
end

Then(/^return "([^"]*)"$/) do |total|
  expect(@total).to eq(total.to_f)
end