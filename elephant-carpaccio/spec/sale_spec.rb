require 'rspec'
require_relative '../model/sale'

describe('Sale') do

  it 'should not apply discount for order below $1000' do
    quantity = 2
    unit_price = 10.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(21.25)
  end

  it 'should apply discount for order between $1000 and $5000' do
    quantity = 10
    unit_price = 100.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(1030.63)
  end

  it 'should apply discount for order between $5000 and $7000' do
    quantity = 60
    unit_price = 100.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(6056.25)
  end

  it 'should apply discount for order between $7000 and $10000' do
    quantity = 80
    unit_price = 100.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(7905.0)
  end

  it 'should apply discount for order between $10000 and $50000' do
    quantity = 40
    unit_price = 1000.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(38250.0)
  end

  it 'should apply discount for order above $50000' do
    quantity = 60
    unit_price = 1000.0
    state_code = 'TX'
    sale = Sale.new(quantity, unit_price, state_code)
    expect(sale.get_total).to eq(54187.5)
  end

end