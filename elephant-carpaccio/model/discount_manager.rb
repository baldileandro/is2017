require_relative '../model/discount0'
require_relative '../model/discount3'
require_relative '../model/discount5'
require_relative '../model/discount7'
require_relative '../model/discount10'
require_relative '../model/discount15'

class DiscountManager
  @@discounts = {
    0...1_000.0 => Discount0.new,
    1_000.0...5000.0 => Discount3.new,
    5_000.0...7_000.0 => Discount5.new,
    7_000.0...10_000.0 => Discount7.new,
    10_000.0...50_000.0 => Discount10.new,
    50_000.0...Float::INFINITY => Discount15.new
  }

  def self.get_discount_percentage(net_amount)
    @@discounts.select{ |amount| amount === net_amount }.values.first
  end
end