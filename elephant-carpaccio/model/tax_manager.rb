require_relative '../model/state_tax_ut'
require_relative '../model/state_tax_nv'
require_relative '../model/state_tax_tx'
require_relative '../model/state_tax_al'
require_relative '../model/state_tax_ca'
require_relative '../model/state_tax'

class TaxManager
  @@taxes = {
      'UT' => StateTaxUT.new,
      'NV' => StateTaxNV.new,
      'TX' => StateTaxTX.new,
      'AL' => StateTaxAL.new,
      'CA' => StateTaxCA.new,
  }
  @@taxes.default = StateTax.new

  def self.get_tax_percentage(state_code)
    @@taxes[state_code]
  end
end