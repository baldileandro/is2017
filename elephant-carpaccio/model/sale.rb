require_relative '../model/discount_manager'
require_relative '../model/tax_manager'


DECIMAL_PRECISION = 2

class Sale
  attr_accessor :quantity, :unit_price, :state_code

  def initialize(quantity, unit_price, state_code)
    @quantity = quantity
    @unit_price = unit_price
    @state_code = state_code
  end

  def get_total
    net_amount = get_net_amount
    discount_coeficient = 1 - (get_discount_percentage(net_amount).discount / 100.0)
    tax_coeficient = 1 + (get_tax_percentage.tax / 100.0)
    (net_amount * discount_coeficient * tax_coeficient).round(DECIMAL_PRECISION)
  end

  private

  def get_discount_percentage(net_amount)
    DiscountManager.get_discount_percentage(net_amount)
  end

  def get_tax_percentage
    TaxManager.get_tax_percentage(@state_code)
  end

  def get_net_amount
    @quantity * @unit_price
  end
end
