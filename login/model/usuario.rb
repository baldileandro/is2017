require_relative '../model/excepcion_password_incorrecto'
require_relative '../model/excepcion_codigo_incorrecto'
require_relative '../model/politica_password'

# Cuenta de usuario, almacena sus datos y permite su autenticacion
class Usuario
  attr_accessor :email, :password, :codigo, :validaciones_fallidas
  def initialize(email)
    @email = email
    @validaciones_fallidas = 0
  end

  def obtener_identificacion
    @email
  end

  def validar_password(password)
    begin
      PoliticaPassword.validar_cantidad_fallos(@validaciones_fallidas)
      password_valida = @password == password
      unless password_valida
        @validaciones_fallidas += 1
      end
      password_valida
    end
  end

  def establecer_codigo(codigo)
    @codigo = codigo
  end

  def validar_codigo(codigo)
    raise ExcepcionCodigoIncorrecto if @codigo != codigo
  end

  def reiniciar_password(codigo, password)
    validar_codigo(codigo)
    establecer_password(password)
  end

  private

  def establecer_password(password)
    PoliticaPassword.validar_password(password)
    @password = password
  end

end
