class ExcepcionUsuarioInexistente < StandardError
  def initialize(msg = 'No existe un usuario con ese email.')
    super
  end
end