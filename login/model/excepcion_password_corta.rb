class ExcepcionPasswordCorta < StandardError
  def initialize(msg = 'El password debe superar la longitud minima.')
    super
  end
end