class ExcepcionPasswordBloqueada < StandardError
  def initialize(msg = 'Se ha bloqueado la password tras superar la cantidad maxima de intentos.')
    super
  end
end