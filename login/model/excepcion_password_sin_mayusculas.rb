class ExcepcionPasswordSinMayusculas < StandardError
  def initialize(msg = 'El password debe contener al menos una mayuscula.')
    super
  end
end