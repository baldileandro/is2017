require_relative '../model/excepcion_password_bloqueada'
require_relative '../model/excepcion_password_sin_mayusculas'
require_relative '../model/excepcion_password_corta'

LONGITUD_MINIMA_PASSWORD = 6
FALLOS_MAXIMOS = 3

# Contiene las reglas refidas a password para homebanking
class PoliticaPassword
  def self.validar_password(password)
    validar_contiene_mayusculas(password)
    validar_longitud(password)
  end

  def self.validar_cantidad_fallos(cantidad)
    raise ExcepcionPasswordBloqueada unless cantidad < FALLOS_MAXIMOS
  end

  private

  def self.validar_contiene_mayusculas(password)
    raise ExcepcionPasswordSinMayusculas unless password.downcase != password
  end

  def self.validar_longitud(password)
    raise ExcepcionPasswordCorta unless password.size >= LONGITUD_MINIMA_PASSWORD
  end
end