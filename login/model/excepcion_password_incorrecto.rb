class ExcepcionPasswordIncorrecto < StandardError
  def initialize(msg = 'El password es incorrecto.')
    super
  end
end