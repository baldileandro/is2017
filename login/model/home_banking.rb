require_relative '../model/usuario'
require_relative '../model/excepcion_usuario_inexistente'

# Repositorio de usuarios
class HomeBanking

  def initialize
    @usuarios = {}
  end

  def agregar_usuario(usuario)
    comprobar_unicidad_usuario(usuario)
    @usuarios[usuario.obtener_identificacion] = usuario
  end

  def obtener_usuario(identificacion)
    @usuarios[identificacion] || raise(ExcepcionUsuarioInexistente)
  end

  private

  def comprobar_unicidad_usuario(usuario)
    raise ExcepcionUsuarioUnico if @usuarios.key?(usuario.obtener_identificacion)
  end

end