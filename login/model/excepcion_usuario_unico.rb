class ExcepcionUsuarioUnico < StandardError
  def initialize(msg = 'Ya existe un usuario registrado con ese email.')
    super
  end
end