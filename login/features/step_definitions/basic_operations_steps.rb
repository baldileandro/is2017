Given(/^my user is "([^"]*)" and password "([^"]*)"$/) do |identificacion, password|
  usuario = Usuario.new(identificacion)
  usuario.establecer_codigo(password)
  usuario.reiniciar_password(password, password)
  @hb = HomeBanking.new
  @hb.agregar_usuario(usuario)
end

When(/^I login with "([^"]*)" and password "([^"]*)"$/) do |identificacion, password|
  usuario_obtenido = @hb.obtener_usuario(identificacion)
  @result = usuario_obtenido.validar_password(password)
end

Then(/^I access the system$/) do
  expect(@result).to eq(TRUE)
end

Then(/^I do not access the system$/) do
  expect(@result).to eq(FALSE)
end

Given(/^the ATM generated code "([^"]*)"$/) do |codigo|
  # Supongo que ya fui al banco y para mi cuenta jd@free.com
  # el ATM me dio un codigo
  usuario = Usuario.new("jd@free.com")
  usuario.establecer_codigo(codigo)
  @hb = HomeBanking.new
  @hb.agregar_usuario(usuario)
end

When(/^I register with email "([^"]*)", password "([^"]*)" and code "([^"]*)"$/) do |identificacion, password, codigo|
  usuario = @hb.obtener_usuario(identificacion)
  begin
    usuario.reiniciar_password(password_temporal, password)
  rescue => @result # ignored
  end
end

Then(/^my account is created$/) do
  # Nada por hacer
end

Then(/^my account is not created$/) do
  expect(@result.is_a? Exception).to eq(TRUE)
end

Given(/^I have already registered$/) do
  @usuario = Usuario.new('example@example.com')
  @usuario.establecer_codigo('Password!')
  @usuario.reiniciar_password('Password!', 'Password!')
end

When(/^I attemp (\d+) failed logins$/) do |arg|
  @usuario.validar_password('Intento 1')
  @usuario.validar_password('Intento 2')
  @usuario.validar_password('Intento 3')
  begin
    @usuario.validar_password('Intento 4')
  rescue ExcepcionPasswordBloqueada => @result # ignored
  end
end

Then(/^my credentials get blocked$/) do
  expect(@result.is_a? ExcepcionPasswordBloqueada).to eq(TRUE)
end