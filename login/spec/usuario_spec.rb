require 'rspec'
require_relative '../model/usuario'

describe('Usuario') do
  it 'Se debe poder crear' do
    email = 'example@example.com'
    Usuario.new(email)
  end

  it 'Se debe identificar por su email' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    expect(usuario.obtener_identificacion).to eq email
  end

  it 'Debe poder reiniciar su password' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    password = 'Password'
    usuario.establecer_codigo(codigo)
    usuario.reiniciar_password(codigo, password)
    expect(usuario.password).to eq(password)
  end

  it 'Debe poder validar su password si es correcta' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    password = 'Password'
    usuario.establecer_codigo(codigo)
    usuario.reiniciar_password(codigo, password)
    usuario.validar_password(password)
    expect(usuario.validar_password(password)).to eq(TRUE)
  end

  it 'Error al intentar validar con password incorrecto' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    password = 'Password'
    usuario.establecer_codigo(codigo)
    usuario.reiniciar_password(codigo, password)
    expect(usuario.validar_password('password incorrecto')).to eq(FALSE)
  end

  it 'Debe poder validar su codigo si es correcta' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    usuario.establecer_codigo(codigo)
    usuario.validar_codigo(codigo)
  end

  it 'Error al intentar validar con codigo incorrecto' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    usuario.establecer_codigo(codigo)
    expect do
      usuario.validar_codigo('codigo incorrecto')
    end.to raise_error(ExcepcionCodigoIncorrecto)
  end

  it 'Debe poder establecer una password_temporal' do
    email = 'example@example.com'
    usuario = Usuario.new(email)
    codigo = 'Password'
    usuario.establecer_codigo(codigo)
    expect(usuario.codigo).to eq(codigo)
  end


end