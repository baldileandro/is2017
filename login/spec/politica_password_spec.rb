require 'rspec'
require_relative '../model/politica_password'

describe 'Politica Password' do

  it 'Error al intentar validar un password sin mayusculas' do
    expect do
      PoliticaPassword.validar_password('hola')
    end.to raise_error(ExcepcionPasswordSinMayusculas)
  end

  it 'Error al intentar validar un password que no supere la longitud minima' do
    expect do
      PoliticaPassword.validar_password('Aa1')
    end.to raise_error(ExcepcionPasswordCorta)
  end

  it 'Debe validar password que cumpla con todos los requisitos' do
    PoliticaPassword.validar_password('Aa112siadhau!98ds767d')
  end

  it 'Error al validar_cantidad_fallos igual al maximo permito' do
    expect do
      PoliticaPassword.validar_cantidad_fallos(4)
    end.to raise_error(ExcepcionPasswordBloqueada)
  end

end