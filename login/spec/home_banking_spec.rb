require 'rspec'
require_relative '../model/home_banking'
require_relative '../model/excepcion_usuario_inexistente'
require_relative '../model/excepcion_usuario_unico'

describe 'Home Banking' do

  it 'Se debe poder crear' do
    HomeBanking.new
  end

  it 'Debe poder almacenar usuarios' do
    usuario_a = double('Usuario A')
    allow(usuario_a).to receive(:obtener_identificacion).and_return('Usuario A')
    usuario_b = double('Usuario B')
    allow(usuario_b).to receive(:obtener_identificacion).and_return('Usuario B')
    hb = HomeBanking.new
    hb.agregar_usuario(usuario_a)
    hb.agregar_usuario(usuario_b)
  end

  it 'Debe poder obtener un usuario almacenado por su identificacion' do
    usuario_a = double('Usuario A')
    allow(usuario_a).to receive(:obtener_identificacion).and_return('Usuario A')
    hb = HomeBanking.new
    hb.agregar_usuario(usuario_a)
    usuario_obtenido = hb.obtener_usuario(usuario_a.obtener_identificacion)
    expect(usuario_obtenido).to eq(usuario_a)
  end

  it 'Error al ontener usuario con identificacion de usuario no existente' do
    usuario_a = double('Usuario A')
    allow(usuario_a).to receive(:obtener_identificacion).and_return('Usuario A')
    hb = HomeBanking.new
    hb.agregar_usuario(usuario_a)
    expect do
      hb.obtener_usuario('identificacion de usuario no existente')
    end.to raise_error(ExcepcionUsuarioInexistente)
  end

  it 'Error al intentar almacenar un usuario que tenga la identificacion de uno ya almacenado' do
    usuario_a = double('Usuario A')
    allow(usuario_a).to receive(:obtener_identificacion).and_return('Usuario A')
    hb = HomeBanking.new
    hb.agregar_usuario(usuario_a)
    expect do
      hb.agregar_usuario(usuario_a)
    end.to raise_error(ExcepcionUsuarioUnico)
  end

end